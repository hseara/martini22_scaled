import math
from optparse import OptionParser

def optP():
    usage="[python3] %prog --itp .itp --oitp .itp"
    description="Changes MARTINI protein itps (appends with 'p')"
    version="\n%prog Version 0.1\n\nRequires Python 3.0 or newer."
    optParser = OptionParser(usage=usage, description=description, version=version)
    optParser.add_option('--itp', type='str', dest='inputfile_itp', help="Martinized itp input")
    optParser.add_option('--oitp', type='str', dest='outputfile_itp', help="Output name")
    return optParser.parse_args()
    
if __name__ == '__main__':

    (options, args) = optP()
    
    name_in_itp=options.inputfile_itp
    name_out_itp=options.outputfile_itp
    name_in_itp="/wrk/hector/programing/repo/ff/martini_scaled/martini_v2.2.itp"
    name_out_itp="/wrk/hector/programing/repo/ff/martini_scaled/martini_v2.2_sigma_epsilon.itp"
    
    flag=0
    
    with open(name_in_itp, 'r') as in_itp, open(name_out_itp, 'w') as out_itp:
        for line in in_itp:
            if "nonbond_params" in line:
                flag = 1
                out_itp.write(line.strip()+"\n")
                continue
            
            if flag == 0 :
                out_itp.write(line.strip()+"\n")
           
            elif flag == 1 :
                if line.strip() == "":
                    out_itp.write("\n")
                elif line.strip()[0] == "[":
                    out_itp.write(line.strip()+"\n")
                    flag = 0
                elif line.strip()[0] == ";" :
                    out_itp.write(line.strip()+"\n")
                else:
                    word=line.strip().split(maxsplit=5)
                    sigma=(float(word[4])/float(word[3]))**(1/6)
                    epsilon=(float(word[3])*float(word[3]))/(4*float(word[4]))
            
                    newline="  {0[0]:<5s} {0[1]:<5s} {0[2]:>5s}   {1:2.3f}   {2:2.4f}  {0[5]}\n"
                    out_itp.write(newline.format(word, sigma, epsilon))
            else:
                print ("ERROR")
         