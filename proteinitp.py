#!/usr/bin/env python3

import re
import math
from optparse import OptionParser

# Call: proteinitp.py --itp 1AKE.itp --oitp 1AKEp.itp

def optP():
    usage="[python3] %prog --itp .itp --oitp .itp"
    description="Changes MARTINI protein itps (appends with 'p')"
    version="\n%prog Version 0.1\n\nRequires Python 3.0 or newer."
    optParser = OptionParser(usage=usage, description=description, version=version)
    optParser.add_option('--itp', type='str', dest='inputfile_itp', help="Martinized itp input")
    optParser.add_option('--oitp', type='str', dest='outputfile_itp', help="Output name")
    return optParser.parse_args()

if __name__ == '__main__':

    (options, args) = optP()
    
    inputfile_itp=options.inputfile_itp
    outputfile_itp=options.outputfile_itp
    
    # Add p's to all protein names in itp
    
    with open(inputfile_itp, 'r') as f:
        with open (outputfile_itp, 'w') as new:
            for line in f:
                splitted=line.strip().split()
                if not splitted or len(splitted)<2 or splitted[0]==';' or splitted[0][0]=='#':
                    new.write(line)
                    continue
                if splitted[1][0]=='P' or splitted[1][0]=='N' or splitted[1][0]=='C' or splitted[1][0]=='S' or splitted[1][0]=='Q' or splitted[1][0]=='A' or splitted[1][0]=='B':
                    pattern="(\\t| )" + str(splitted[1])+"(\\t| )"
                    new.write(re.sub(pattern, " "+splitted[1]+"p ", line))
                else:
                    new.write(line)
                    
    